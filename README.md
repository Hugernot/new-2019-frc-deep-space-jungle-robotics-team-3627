# 2019 FRC Deep Space Jungle Robotics Team 3627

## Prerequisites for Running Code

* Install packages listed here https://robotpy.readthedocs.io/en/2019.0.2/install/index.html
* Install my console utility package https://pypi.org/project/pyconsole-util/ "pip install pyconsole-util"
* For deploying to the Robot, the roboRIO needs to be imaged, updated, and have robotpy installed. https://robotpy.readthedocs.io/en/2019.0.2/install/robot.html

If you are on the team this year and would like to learn how to participate, check the Discord's info channel for getting started.

## Goals

