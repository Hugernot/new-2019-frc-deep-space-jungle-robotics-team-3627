import cv2, os

cap = cv2.VideoCapture(0)

while True:
    ret, img = cap.read()
    cv2.imwrite("/tmp/tmp.jpg", img)
    os.rename("/tmp/tmp.jpg", "/tmp/stream1.jpg")

    onlyfiles = next(os.walk('/mnt/data/front'))[2] #dir is your directory path as string
    cv2.imwrite("/mnt/data/front/"+str(len(onlyfiles))+".jpg", img)

    key = cv2.waitKey(10)
    if key == 27:
        break


cv2.destroyAllWindows()
cv2.VideoCapture(0).release()
