from wpilib import AnalogPotentiometer

class RobotPotentiometer(AnalogPotentiometer):
    def __init__(self, channel, actual_angle_per_reading=0.3, angle_offset=0):
        super().__init__(channel)
        self.actual_angle_per_reading = actual_angle_per_reading
        self.angle_offset = angle_offset

    def getAngle(self):
        return ((self.get() / self.actual_angle_per_reading) * 90) + self.angle_offset