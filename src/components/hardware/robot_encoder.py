from wpilib import Encoder

class RobotEncoder(Encoder):
    def __init__(self, channel_a, channel_b, angle_offset=0):
        super().__init__(channel_a, channel_b)
        self.angle_offset=angle_offset

    def getAngle(self):
        return self.get() + self.angle_offset