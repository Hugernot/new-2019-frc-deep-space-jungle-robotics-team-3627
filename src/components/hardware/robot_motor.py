import ctre, time

accel_fact = 0.01

class RobotMotor:
    def __init__(self, channel):
        self.motor = ctre.WPI_TalonSRX(channel)
        self.set_speed = 0.0
        self.cur_speed = 0.0
        self.last_time = time.time()

    def set(self,speed):
        self.set_speed = speed

    def get(self):
        return self.set_speed

    def update(self):
        cur_time = time.time()
        time_dif = (cur_time - self.last_time) * 1000
        self.cur_speed += (self.set_speed-self.cur_speed) * accel_fact * time_dif
        self.motor.set(self.cur_speed)
        self.last_time = cur_time

    def setInverted(self, bool):
        self.motor.setInverted(bool)

    def stopMotor(self):
        self.motor.set(0)
