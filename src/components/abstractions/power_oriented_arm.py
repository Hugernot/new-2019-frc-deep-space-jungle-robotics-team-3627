from components.abstractions import RobotArm

class PowerOrientedArm(RobotArm):

    def __init__(self, robot, angling_motors, encoder=None, potentiometer=None, min_angle=-360, max_angle=360,
    angle_power_up=1, angle_power_down=1, anchor_speed=0.1):

        super().__init__(
            robot, angling_motors=angling_motors, encoder=encoder, potentiometer=potentiometer, min_angle=min_angle, max_angle=max_angle,
        )

        # Used for different powers up and down (usually down is less because of gravity)
        self.angle_power_up = angle_power_up
        self.angle_power_down = angle_power_down

        # Used for keeping the arm in place against the force of gravity with a constant power, crude but works
        self.anchor_speed = anchor_speed

    def rawRotate(self, speed):
        for _, motor in self.angling_motors.items():
            motor.set(speed)

    def rotate(self, speed):
        if self.angle != None:
            # Arm is going past its max or min points, don't adjust
            if ((speed > 0 and self.angle >= self.max_angle)
            or (speed < 0 and self.angle <= self.min_angle)):
                return

        if speed < 0: self.rawRotate(speed*self.angle_power_down)
        else: self.rawRotate(speed*self.angle_power_up)

    def anchor(self):
        self.rawRotate(self.anchor_speed)

    def operate(self):
        user_input = self.robot.controller.left_trigger - self.robot.controller.right_trigger

        if user_input == 0: self.anchor()
        else: self.rotate(user_input)
