from components.abstractions import RobotSubsystem
from control import PIDController

class RobotArm(RobotSubsystem):

    # Two types: Power oriented and Angle Oriented
    '''
    Angling motors are a requirement

    encoders, potentiometers, min and max angles are important for both types

    The angle powers and anchor speeds are important for a power oriented arm

    The tolerance is important for an angle based arm
    '''

    def __init__(self, robot, angling_motors: dict, encoder=None, potentiometer=None, min_angle=-360, max_angle=360):

        self.angling_motors = angling_motors # Motors used for angling

        # Either and encoder or a potentiometer must be given for using angles
        self.encoder = encoder
        self.potentiometer = potentiometer

        # Angles are 0 at the ground, positive upward, and negative downward https://www.mathsisfun.com/angles.html
        self.min_angle = min_angle
        self.max_angle = max_angle


        self.angle = None
        # Has an encoder
        if self.encoder != None:
            self.angle = self.encoder.getAngle()
        # Has a potentiometer
        elif self.potentiometer != None:
            self.angle = self.potentiometer.getAngle()

        super().__init__(robot)

    def getAngle(self):
        self.update()
        if self.angle == None:
            raise Exception("Cannot use getAngle, arm does not have an encoder or potentiometer")
    
        return self.angle
    
    def update(self):
        # Has an encoder
        if self.encoder != None:
            self.angle = self.encoder.getAngle()
        # Has a potentiometer
        elif self.potentiometer != None:
            self.angle = self.potentiometer.getAngle()
