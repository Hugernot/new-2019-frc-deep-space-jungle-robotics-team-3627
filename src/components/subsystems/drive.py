import wpilib, ctre, math
from wpilib.drive import MecanumDrive
from components.hardware import RobotMotor
from components.abstractions import RobotSubsystem
from control import PIDController

class Drive(RobotSubsystem):

    def __init__(self, robot):
        super().__init__(robot)

        self.default_power_coefficient = 0.75
        self.assist_power_coefficient = 0.45
        self.is_high_power = False

        # Joystick power curve values
        self.curve_exponent = 1.75
        self.power_coefficient = self.default_power_coefficient

        # The angles on the field of game elements
        self.anglesnaps = [0, 28.75, 90, 180-28.75, 180, 180+28.75, 270, 360-28.75, 360]
        # Starts the assist power values at 0.20 to avoid motor deadzone
        self.assist_deadband = 0.20

        self.strafe_assist_controller = PIDController(1/180, 0, 0, -1, 1) # Kp, Ki, Kd, min output, max output
        self.steer_assist_controller = PIDController(1/180, 0, 0, -1, 1) # Kp, Ki, Kd, min output, max output
        # PIDControllers return degree targets. (use Power * Seconds/Degrees to cancel out)

        # Motor declaration (from robotmap)
        self.front_left_motor = RobotMotor(robot.map.can.drive.DRIVE_FRONT_LEFT_WHEEL)
        self.front_right_motor = RobotMotor(robot.map.can.drive.DRIVE_FRONT_RIGHT_WHEEL)
        self.back_left_motor = RobotMotor(robot.map.can.drive.DRIVE_BACK_LEFT_WHEEL)
        self.back_right_motor = RobotMotor(robot.map.can.drive.DRIVE_BACK_RIGHT_WHEEL)
        
        # Setting inversions
        self.back_left_motor.setInverted(True)
        self.back_right_motor.setInverted(True)

        # Make mecanum drive object
        self.controller = wpilib.drive.MecanumDrive(
            self.front_left_motor,
            self.back_left_motor,
            self.front_right_motor,
            self.back_right_motor
        )
        self.controller.setExpiration(0.1)

        # Initialize network tables values
        robot.vision_network_table.putString("Direction", "back")

        robot.vision_network_table.putNumber("Target X", 0)
        robot.vision_network_table.putNumber("Target Y", 0)

        robot.vision_network_table.putBoolean("Targeting Enabled", False)
        robot.vision_network_table.putBoolean("Targets Detected", False)
        
        robot.vision_network_table.putNumber("Target Yaw", 0)
        robot.vision_network_table.putNumber("Target Distance", 0)

    def getClosestAnglesnap(self, current_angle):
        closest_angle = 0
        closest_angle_error = None
        for angle in self.anglesnaps:
            angle_difference = abs(current_angle%360 - angle)
            if not closest_angle_error or angle_difference < closest_angle_error:
                closest_angle_error = angle_difference
                closest_angle = angle
        return closest_angle

    def controlDrive(self, robot):
         # Default power coefficient just for running in the loop <3
        power_coefficient_adjusted = self.power_coefficient

        # Values directly added to steer and strafe
        assist_strafe = 0
        assist_steer = 0
        assist_compass = 0

        gyroAngle = robot.gyro.getAngle() # Gets the gyro angle of the robot to compensate for error
        # print(gyroAngle)

        # If mode was just switch or aim assist just enabled then
        if robot.controller.x_just_pressed or robot.controller.b_just_pressed:
            # Reset controllers for axes
            pass

        targeting_enabled = robot.controller.x_pressed_again or robot.controller.b_pressed_again # Targeting is enabled if the buttons are pressed again
        targets_detected = robot.vision_network_table.getBoolean("Targets Detected", False)

        robot.vision_network_table.putString("Direction", "front" if robot.direction == 1 else "back")
        # robot.vision_network_table.putBoolean("Targeting Enabled", targeting_enabled)
        # Assist loop runs when either x is pressed again or b is pressed again (mode buttons)
        if targeting_enabled:
            target_yaw = robot.vision_network_table.getNumber("Target Yaw", 0)

            # Assist power coefficient will always override high power/low power driving mode
            power_coefficient_adjusted = self.assist_power_coefficient

            # if targets_detected == False:
                # Reset controllers for axes
                # pass

        # Power curve adjusted stick inputs goes into drive
        left_stick_y_curved = powerCurve(robot.controller.left_stick_y, power_coefficient_adjusted, self.curve_exponent)
        left_stick_x_curved = powerCurve(robot.controller.left_stick_x, power_coefficient_adjusted,self.curve_exponent)
        right_stick_x_curved = powerCurve(robot.controller.right_stick_x, power_coefficient_adjusted, self.curve_exponent)
        right_stick_y_curved = powerCurve(robot.controller.right_stick_y, power_coefficient_adjusted, self.curve_exponent)

        assist_strafe = deadBand(assist_strafe, self.assist_deadband)
        assist_steer = deadBand(assist_steer, self.assist_deadband)
        assist_compass = deadBand(assist_compass, self.assist_deadband)

        self.controller.driveCartesian(
            (right_stick_x_curved + assist_strafe), # Steer movement
            -left_stick_y_curved * robot.direction, # Forward movement
            ((left_stick_x_curved + assist_steer + assist_compass) * robot.direction) * 1.8, # Horizontal movement
            0
        )

        # Put into network table for external reference
        robot.main_network_table.putBoolean("High Power Mode", self.is_high_power)

    def setPower(self, desired_power):
        self.is_high_power = desired_power != 1
        self.power_coefficient = self.default_power_coefficient * desired_power

    def update(self):
        self.front_left_motor.update()
        self.back_left_motor.update()
        self.front_right_motor.update()
        self.back_right_motor.update()

    def disable(self):
        self.controller.driveCartesian(0,0,0,0)

def powerCurve(input_value, coefficient, exponent):
    if input_value == 0: return 0
    elif input_value > 0: return abs(input_value**exponent)*coefficient
    else: return -abs(input_value**exponent)*coefficient

def deadBand(input_value, deadzone):
    if input_value == 0: return 0
    elif input_value > 0: return abs(input_value) * (1-deadzone) + deadzone
    else: return -abs(input_value) * (1-deadzone) - deadzone
