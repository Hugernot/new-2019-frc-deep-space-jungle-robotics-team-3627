import ctre, wpilib
from components.abstractions import AngleOrientedArm # PowerOrientedArm
from components.hardware import RobotPotentiometer

retracted = 2
extended = 1

def translate(value, leftMin, leftMax, rightMin, rightMax):
    leftSpan = leftMax - leftMin
    rightSpan = rightMax - rightMin

    valueScaled = float(value - leftMin) / float(leftSpan)

    return rightMin + (valueScaled * rightSpan)

class HatchArmAngle(AngleOrientedArm):

    is_folded = True

    def __init__(self, robot):
        
        self.puncher = wpilib.DoubleSolenoid(
            forwardChannel=robot.map.pcm.PUNCHER_FORWARD, reverseChannel=robot.map.pcm.PUNCHER_BACKWARD
        )

        super().__init__(
            robot,
            angling_motors = {"hatch_arm_angling_motor": ctre.WPI_VictorSPX(robot.map.can.HATCH_ARM_ANGLER)},
            positions=[172, 88, -8],
            potentiometer=RobotPotentiometer(robot.map.analog.HATCH_ARM_POTENTIOMETER)
        )

    def unfold(self):
        self.position = 1
        if (self.angle > self.positions[1] - 10) and (self.angle < self.positions[1] + 10):
            self.is_folded = False

    def punch(self):
        # Don't extend, arm is too far downward
        if self.getAngle() < self.min_angle: return
        
        self.puncher.set(extended)

    def retract(self):
        self.puncher.set(retracted)

    def operate(self):
        if self.robot.controller.a_pressed: self.punch()
        else: self.retract()

        if self.robot.controller.right_bumper_just_pressed: self.nextPosition()
        elif self.robot.controller.left_bumper_just_pressed: self.prevPosition()
    
    def setAngle(self, target_angle): # 90 degrees = 100% power, error/90
        Kp = 4.5
        error = target_angle - self.angle # 90 - 180, -90
        # print("setAngle:", target_angle, "current angle:", self.angle)
        if self.angle < 90:
            self.rawRotate((Kp * error)/90 + 0.1)
        else:
            self.rawRotate((Kp * error)/90)

    def update(self):
        # Has an encoder
        if self.encoder != None:
            self.angle = self.encoder.getAngle()
        # Has a potentiometer
        elif self.potentiometer != None:
            self.angle = translate(self.potentiometer.get(), 0.403, 1.006, 0, 171)
        
        print("Hatch arm angle:", self.angle, "\nHatch position:", self.position, "\nHatch target angle:", self.positions[self.position], "\n\n")

        self.setAngle(self.positions[self.position])
