import wpilib, ctre
from components.abstractions import AngleOrientedArm
from components.hardware import RobotEncoder, RobotPotentiometer

class BallArm(AngleOrientedArm):

    is_folded = True

    def __init__(self, robot):

        self.roller = ctre.WPI_VictorSPX(robot.map.can.BALL_ARM_INTAKE)
        self.roller.setInverted(True)
        
        right_motor = ctre.WPI_VictorSPX(robot.map.can.BALL_ARM_ANGLER_RIGHT)
        left_motor = ctre.WPI_VictorSPX(robot.map.can.BALL_ARM_ANGLER_LEFT)
        right_motor.setInverted(True)
        left_motor.setInverted(True)

        super().__init__(
            robot,
            angling_motors={
                "left_angle_motor": left_motor,
                "right_angle_motor": right_motor 
            },
            # encoder=RobotEncoder(
            #     robot.map.dio.BALL_ARM_ENCODER_A,
            #     robot.map.dio.BALL_ARM_ENCODER_B,
            #     angle_offset=180 # Starts folded up
            # ),
            max_power_up=0.2,
            max_power_down=0.2,
            # gravity_assist=0.6,
            balanced_angle=105,
            # anchor_speed=0.1, # PowerOrientedArm
            min_angle=-5,
            max_angle=200,
            # anchor_speed=0
            tolerance=0,

            positions=[155, 90, 60, -10],
            potentiometer=RobotPotentiometer(robot.map.analog.BALL_ARM_POTENTIOMETER, actual_angle_per_reading=0.41, angle_offset=-24.7),
        )

        self.pid.Kp = 0.6
        self.pid.Ki = 0.45
        self.pid.Kd = 0.55

        del right_motor
        del left_motor

    def unfold(self):
        # print("Ball angle", self.getAngle())
        # if self.getAngle() > 120: self.rotate(-0.3)
        # else: self.is_folded = False
        self.position = 2
        if (self.angle > self.positions[2] - 10) and (self.angle < self.positions[2] + 10):
            self.is_folded = False
    
    def rollIntake(self, power):
        self.roller.set(power)

    def operate(self):
        ## Old intake code
        # if self.robot.controller.right_bumper_pressed: self.rollIntake(1)
        # elif self.robot.controller.left_bumper_pressed: self.rollIntake(-0.30)
        # else: self.rollIntake(0)

        user_input =  self.robot.controller.right_trigger - self.robot.controller.left_trigger
        self.rollIntake(
            max(
                min(user_input, 0.9),
                -0.3
            )
        )
        if self.robot.controller.right_bumper_just_pressed:
            self.nextPosition()
        elif self.robot.controller.left_bumper_just_pressed:
            self.prevPosition()

        # print("user_input:", user_input)
        ## Motor testing
        # self.rawRotate(user_input)
        # self.angling_motors["right_angle_motor"].set(user_input)
        # self.angling_motors["left_angle_motor"].set(user_input)

    def setAngle(self, target_angle): # 90 degrees = 100% power, error/90
        Kp = 3
        error = target_angle - self.angle # 90 - 180, -90
        # print("setAngle:", target_angle, "current angle:", self.angle)
        self.rawRotate((Kp * error)/90)

    def update(self):
        # Has an encoder
        if self.encoder != None:
            self.angle = self.encoder.getAngle()
        # Has a potentiometer
        elif self.potentiometer != None:
            self.angle = self.potentiometer.getAngle()
        
        # print("update:", self.angling_motors['left_angle_motor'].get(), "\n\n")
            
        # self.setAngle(self.positions[self.position], self.robot.delta_time)
        self.setAngle(self.positions[self.position])
        # print("in ball_arm.target:", self.positions[self.position], "current angle:", self.angle, "delta_time:", self.robot.delta_time)


        # self.rotate(user_input)
        # self.setAngle(self.robot.controller.left_trigger * -90 + 90, 0.1)
